package com.company;

public class Producer implements Runnable{

    private final UpdateCostInterface update;

    public Producer(UpdateCostInterface update) {
        this.update = update;
    }

    @Override
    public void run() {
        while (true){
            try {
                Thread.sleep((int) (Math.random() * 10000));
                if (Math.random() > 0.5f) {
                    update.updateCost((int) (Math.random() * 10000));
                }
            } catch (Exception e) {
                System.out.println("Error" + e.getMessage());
            }
        }
    }
}
