package com.company;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Consumer implements Runnable{
    String good = "%s консьюмер успешно провел операцию типа %d с  %s в количестве %d";
    String bad = "%s консьюмер НЕуспешно провел операцию типа %d с  %s в количестве %d";
    private final Exchange exchange;
    private Integer balance = 10000;
    private Integer paper_balance = 10;
    private Integer futures_balance = 10;
    private Integer actions_balance = 10;

    public Consumer(Exchange exchange) {
        this.exchange = exchange;
    }

    @Override
    public void run() {
        while(true) { // так делать плохо, но пофиг
            try {
                Thread.sleep((int) (Math.random() * 10000));
                if (Math.random() > 0.5f) {
                    int do_count = (int) (Math.random() * 100);
                    int type = Math.random() > 0.5f ? 1 : 0;
                    switch ((int) (Math.random() * 3)) {
                        case 0 -> {
                            if (exchange.doPaper(this, do_count, type)) {
                                System.out.printf((good) + "%n", Thread.currentThread().getName(), type, "ценными бумагами", do_count);
                            } else {
                                System.out.printf((bad) + "%n", Thread.currentThread().getName(), type, "ценными бумагами", do_count);
                            }
                        }
                        case 1 -> {
                            if (exchange.doActions(this, do_count, type)) {
                                System.out.printf((good) + "%n", Thread.currentThread().getName(), type, "акциями", do_count);
                            } else {
                                System.out.printf((bad) + "%n", Thread.currentThread().getName(), type, "акциями", do_count);
                            }
                        }
                        case 2 -> {
                            if (exchange.doFutures(this, do_count, type)) {
                                System.out.printf((good) + "%n", Thread.currentThread().getName(), type, "фьючерсами", do_count);
                            } else {
                                System.out.printf((bad) + "%n", Thread.currentThread().getName(), type, "фьючерсами", do_count);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                System.out.println("Error" + e.getMessage());
            }
        }
    }
}
