package com.company;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Exchange exch = new Exchange();
        UpdateCostInterface updateActions = cost -> {
            exch.actions_cost.set(cost);
            System.out.println(Thread.currentThread().getName() + " update actions cost to " + cost);
        };

        UpdateCostInterface updateFutures = cost -> {
            exch.futures_cost.set(cost);
            System.out.println(Thread.currentThread().getName() + " update futures cost to " + cost);
        };

        UpdateCostInterface updatePapers = cost -> {
            exch.paper_cost.set(cost);
            System.out.println(Thread.currentThread().getName() + " update papers cost to " + cost);
        };

        new Thread(new Producer(updateActions)).start();
        new Thread(new Producer(updateActions)).start();
        new Thread(new Producer(updateActions)).start();

        new Thread(new Producer(updateFutures)).start();
        new Thread(new Producer(updateFutures)).start();
        new Thread(new Producer(updateFutures)).start();

        new Thread(new Producer(updatePapers)).start();
        new Thread(new Producer(updatePapers)).start();
        new Thread(new Producer(updatePapers)).start();

        new Thread(new Consumer(exch)).start();
        new Thread(new Consumer(exch)).start();
        new Thread(new Consumer(exch)).start();
        new Thread(new Consumer(exch)).start();
        new Thread(new Consumer(exch)).start();
        new Thread(new Consumer(exch)).start();
        new Thread(new Consumer(exch)).start();

        Thread.sleep(60_000);
    }
}