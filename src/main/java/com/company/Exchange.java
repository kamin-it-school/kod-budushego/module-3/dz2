package com.company;

import java.util.concurrent.atomic.AtomicInteger;

public class Exchange {
    public volatile AtomicInteger paper_cost = new AtomicInteger(100);
    public volatile AtomicInteger futures_cost = new AtomicInteger(100);
    public volatile AtomicInteger actions_cost = new AtomicInteger(100);
    
    public boolean doPaper(Consumer cons,Integer do_count,int operation){
        int cost = paper_cost.get();
        if(operation == 1){
            if(do_count <= cons.getPaper_balance()){
                cons.setPaper_balance(cons.getPaper_balance() - do_count);
                cons.setBalance(do_count*cost + cons.getBalance());
            }else{
                return false;
            }
        }else{
            if(do_count*cost <= cons.getBalance()){
                cons.setPaper_balance(cons.getPaper_balance() + do_count);
                cons.setBalance(cons.getBalance() - do_count*cost);
            }else{
                return false;
            }
        }
        return true;
    }

    public boolean doFutures(Consumer cons,Integer do_count,int operation){
        int cost = futures_cost.get();
        if(operation == 1){
            if(do_count <= cons.getFutures_balance()){
                cons.setFutures_balance(cons.getFutures_balance() - do_count);
                cons.setBalance(do_count*cost + cons.getBalance());
            }else{
                return false;
            }
        }else{
            if(do_count*cost <= cons.getBalance()){
                cons.setFutures_balance(cons.getFutures_balance() + do_count);
                cons.setBalance(cons.getBalance() - do_count*cost);
            }else{
                return false;
            }
        }
        return true;
    }

    public boolean doActions(Consumer cons,Integer do_count,int operation){
        int cost = actions_cost.get();
        if(operation == 1){
            if(do_count <= cons.getActions_balance()){
                cons.setActions_balance(cons.getActions_balance() - do_count);
                cons.setBalance(do_count*cost + cons.getBalance());
            }else{
                return false;
            }
        }else{
            if(do_count*cost <= cons.getBalance()){
                cons.setActions_balance(cons.getActions_balance() + do_count);
                cons.setBalance(cons.getBalance() - do_count*cost);
            }else{
                return false;
            }
        }
        return true;
    }

}
